<?php
use App\User;
use App\Post;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('posts', function(){
	return Post::published()->get();
});

Route::get('posts/unpublished', function(){
	return Post::unpublished()->get();
})->middleware('can:editor');

Route::get('posts/me', function(){
	return auth()->user()->posts()->get();
})->middleware('can:author_or_editor');

Route::get('post/{post}', function(Post $post){
	if($post->published || (auth()->user() && auth()->user()->can('view', $post))){
		return $post;
	}
	abort(403);
});

Route::get('user/{user}', function(User $user){
	return $user;
});

Route::get('me', function(){
	return auth()->user();
})->middleware('auth');

Route::get('author', function(){
	return 'ok';
})->middleware('can:author');

Route::get('editor', function(){
	return 'ok';
})->middleware('can:editor');

Route::get('two', function(){
	return 'ok';
})->middleware('can:author_editor');

Route::get('either', function(){
	return 'ok';
})->middleware('can:author_or_editor');