<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\User;
use App\Post;
use App\Policies\PostPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Post::class => PostPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $this->registerUserPolicies();
        //
    }

    protected function registerUserPolicies()
    {
        Gate::define('author', function($user){
            return $user->hasRole('author');
        });

        Gate::define('editor', function($user){
            return $user->hasRole('editor');
        });

        Gate::define('author_editor', function($user){
            return $user->hasRoles('author', 'editor');
        });

        Gate::define('author_or_editor', function($user){
            return $user->hasAnyRoles('author', 'editor');
        });
    }
}
