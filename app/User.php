<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['roles'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function posts()
    {
        return $this->hasMany(Post::class, 'author_id');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function hasRole($role)
    {
        return !! $this->roles()->whereName($role)->first();
    }

    public function hasRoles(... $args)
    {
        foreach($args as $role){
            if(! $this->hasRole($role)){
                return false;
            }
        }
        return true;
    }

    public function hasAnyRoles(... $args)
    {
        foreach($args as $role){
            if($this->hasRole($role)){
                return true;
            }
        }
        return false;
    }

    public function toArray()
    {
        $array = parent::toArray();
        $array['roles'] = $this->roles->pluck('name')->all();
        return $array;
    }
}
