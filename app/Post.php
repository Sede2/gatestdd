<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function scopePublished($query, $user = null)
    {
    	return $query->wherePublished(true);
    }

    public function scopeUnpublished($query)
    {
    	return $query->wherePublished(false);
    }

    public function scopeAuthoredBy($query, User $user)
    {
    	return $query->where('author_id', $user->id);
    }

    public function authoredBy(User $user)
    {
    	return $this->author_id == $user->id;
    }
}
