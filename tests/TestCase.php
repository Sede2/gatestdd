<?php

namespace Tests;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use App\User;
use App\Role;
use App\Post;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use DatabaseMigrations;

    public function create_user(... $args)
    {
    	$roles = [];
    	foreach($args as $role){
    		$roles[] = $this->create_role($role);
    	}
    	$roles = collect($roles);

    	$user = factory(User::class)->create();

    	$user->roles()->sync($roles->pluck('id')->all());
    	return $user;
    }

    public function create_role($role)
    {
    	if($r = Role::whereName($role)->first()){
    		return $r;
    	}
    	return factory(Role::class)->create(['name' => $role]);
    }

    public function create_posts(User $user, $published, $num = 1)
    {
    	$posts = factory(Post::class, $num)->create(['author_id' => $user->id, 'published' => $published]);
    	if($num == 1){
    		return $posts->first();
    	}
    	return $posts;
    }
}
