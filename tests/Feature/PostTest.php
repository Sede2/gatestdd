<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Post;
use App\User;
use App\Role;

class PostTest extends TestCase
{

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_posts_unpublished_are_not_listed()
    {
    	$user = $this->create_user();
        $posts = $this->create_posts($user, false, 50);

        $response = $this->get('/posts');
        foreach($posts as $post){
        	$response->assertDontSee($post->title);
        }
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_posts_published_are_listed()
    {
    	$user = $this->create_user();
        $posts = $this->create_posts($user, true, 50);

        $response = $this->get('/posts');
        foreach($posts as $post){
        	$response->assertSee($post->title);
        }
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_posts_unpublished_are_listed_for_editor()
    {
    	$user = $this->create_user('editor');
    	$user2 = $this->create_user();
    	$posts = $this->create_posts($user, false, 25);
    	$posts2 = $this->create_posts($user, true, 25);

        $this->get('/posts/unpublished')->assertStatus(302);

        $this->actingAs($user2);
        $this->get('/posts/unpublished')->assertStatus(403);

        $this->actingAs($user);
        $response = $this->get('/posts/unpublished');
        foreach($posts as $post){
        	$response->assertSee($post->title);
        }
        foreach($posts2 as $post){
        	$response->assertDontSee($post->title);
        }
    }

    public function test_author_can_see_own_posts()
    {
    	$user = $this->create_user('author');
    	$user2 = $this->create_user();
    	$user3 = $this->create_user('author');

    	$posts = $this->create_posts($user, false, 25);
    	$posts2 = $this->create_posts($user, true, 25);

        $this->get('/posts/me')->assertStatus(302);

        $this->actingAs($user2);
        $this->get('/posts/me')->assertStatus(403);

        $this->actingAs($user);
        $response = $this->get('/posts/me');
        $response->assertStatus(200);
        foreach($posts as $post){
        	$response->assertSee($post->title);
        }
        
        foreach($posts2 as $post){
        	$response->assertSee($post->title);
        }

        $this->actingAs($user3);
        $response = $this->get('/posts/me');
        $response->assertStatus(200);
        foreach($posts as $post){
        	$response->assertDontSee($post->title);
        }
        
        foreach($posts2 as $post){
        	$response->assertDontSee($post->title);
        }
    }

    public function test_guest_user_can_access_published_post()
    {
    	$user = $this->create_user();
    	$post = $this->create_posts($user, true);

    	$response = $this->get('/post/' . $post->id);
    	$response->assertStatus(200);
    	$response->assertSee($post->title);
    }

    public function test_guest_user_get_redirected_on_unpublished_post()
    {
    	$user = $this->create_user();
    	$post = $this->create_posts($user, false);

    	$response = $this->get('/post/' . $post->id);
    	$response->assertStatus(403);
	}

	public function test_user_can_access_published_post()
	{
		$user = $this->create_user();
		$user2 = $this->create_user();
		$post = $this->create_posts($user, true);

		$this->actingAs($user2);
    	$response = $this->get('/post/' . $post->id);
    	$response->assertStatus(200);
    	$response->assertSee($post->title);
    }

    public function test_user_cannot_access_unpublished_post()
    {
		$user = $this->create_user();
		$user2 = $this->create_user();
    	$post = $user = $this->create_posts($user, false);
    	
    	$response = $this->get('/post/' . $post->id);
    	$response->assertStatus(403);
    }

    public function test_author_can_see_own_post()
    {
    	$user = $this->create_user();
    	$post = $this->create_posts($user, false);
    	$post2 = $this->create_posts($user, true);

    	$this->actingAs($user);
    	$response = $this->get('/post/' . $post->id);
    	$response->assertStatus(200);
    	$response->assertSee($post->title);

		$this->actingAs($user);
    	$response = $this->get('/post/' . $post2->id);
    	$response->assertStatus(200);
    	$response->assertSee($post2->title);
    }

    public function test_editor_can_see_other_post()
    {
    	$user = $this->create_user();
    	$user2 = $this->create_user('editor');

    	$post = $this->create_posts($user, false);
    	$post2 = $this->create_posts($user, true);

    	$this->actingAs($user2);
    	$response = $this->get('/post/' . $post->id);
    	$response->assertStatus(200);
    	$response->assertSee($post->title);

		$this->actingAs($user2);
    	$response = $this->get('/post/' . $post2->id);
    	$response->assertStatus(200);
    	$response->assertSee($post2->title);
    }

    public function test_author_cannot_see_other_unpublished_post()
    {
    	$user = $this->create_user();
    	$user2 = $this->create_user('author');
    	$post = $this->create_posts($user, false);

    	$this->actingAs($user2);
    	$response = $this->get('/post/' . $post->id);
    	$response->assertStatus(403);
    }
    public function test_author_cannot_see_other_published_post()
    {
    	$user = $this->create_user();
    	$user2 = $this->create_user('author');
    	$post = $this->create_posts($user, true);


		$this->actingAs($user2);
    	$response = $this->get('/post/' . $post->id);
    	$response->assertStatus(200);
    	$response->assertSee($post->title);
    }
}
