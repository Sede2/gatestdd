<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;
use App\Role;

class UserHasRolesTest extends TestCase
{

    public function test_user_has_roles()
    {
        $user = $this->create_user('author', 'editor');
        $user2 = $this->create_user();
        $user3 = $this->create_user('author');
        $user4 = $this->create_user('editor');
       
        $response = $this->get('/user/' . $user->id);
        $response->assertSee('author');
        $response->assertSee('editor');
        $response->assertSee($user->name);

        $response = $this->get('/user/' . $user2->id);
        $response->assertDontSee('author');
        $response->assertDontSee('editor');
        $response->assertSee($user2->name);

        $this->actingAs($user);
        $response = $this->get('/me');
        $response->assertSee('author');
        $response->assertSee('editor');
        $response->assertSee($user->name);

        $this->actingAs($user3);
        $response = $this->get('/me');
        $response->assertSee('author');
        $response->assertDontSee('editor');
        $response->assertSee($user3->name);

        $this->actingAs($user4);
        $response = $this->get('/me');
        $response->assertDontSee('author');
        $response->assertSee('editor');
        $response->assertSee($user4->name);
    }

    public function test_user_author_can_visit_only_author()
    {
        $user = $this->create_user('author');
        
        $this->actingAs($user);
        $this->get('author')->assertStatus(200);
    }

    public function test_user_not_author_cannot_visit_only_author()
    {
    	$user = $this->create_user('editor');

        $this->actingAs($user);
        $this->get('author')->assertStatus(403);
    }

    public function test_user_editor_can_visit_only_author()
    {
    	$user = $this->create_user('editor');

        $this->actingAs($user);
        $this->get('editor')->assertStatus(200);
    }

    public function test_user_not_editor_cannot_visit_only_author()
    {
    	$user = $this->create_user('author');
        $user2 = $this->create_user();

        $this->actingAs($user);
        $this->get('editor')->assertStatus(403);

        $this->actingAs($user2);
        $this->get('editor')->assertStatus(403);
    }

    public function test_user_needs_two_roles()
    {
        $user = $this->create_user('editor');
        $user2 = $this->create_user('author', 'editor');
        $user3 = $this->create_user();

        $this->actingAs($user);
        $this->get('two')->assertStatus(403);

        $this->actingAs($user3);
        $this->get('two')->assertStatus(403);

        $this->actingAs($user2);
        $this->get('two')->assertStatus(200);
    }

    public function test_user_needs_either_editor_or_author_role()
    {
        $user = $this->create_user();
        $user2 = $this->create_user('editor');
        $user3 = $this->create_user('author');
        $user4 = $this->create_user('author', 'editor');

        $this->get('either')->assertStatus(302);

        $this->actingAs($user);
        $this->get('either')->assertStatus(403);

        $this->actingAs($user2);
        $this->get('either')->assertStatus(200);

        $this->actingAs($user3);
        $this->get('either')->assertStatus(200);

        $this->actingAs($user4);
        $this->get('either')->assertStatus(200);
    }
}
